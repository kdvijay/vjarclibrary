//
//  VJArcLibrary.swift
//  VJArcLibrary
//
//  Created by Dharmarajan Krishnamoorthy (VJ) on 5/1/20.
//  Copyright © 2020 Excentus Corporation. All rights reserved.
//

import Foundation
public final class VJArcLibrary {

    let name = "VJArcLibrary"
    
    public func add(a: Int, b: Int) -> Int {
        return a + b
    }
    
    public func sub(a: Int, b: Int) -> Int {
        return a - b
    }
    
}
