//
//  VJArcLibraryTests.swift
//  VJArcLibraryTests
//
//  Created by Dharmarajan Krishnamoorthy (VJ) on 5/1/20.
//  Copyright © 2020 Excentus Corporation. All rights reserved.
//

import XCTest
@testable import VJArcLibrary

class VJArcLibraryTests: XCTestCase {

    var vjArcLibrary: VJArcLibrary!

    override func setUp() {
        vjArcLibrary = VJArcLibrary()
    }

    func testAdd() {
        XCTAssertEqual(vjArcLibrary.add(a: 1, b: 1), 2)
    }
    
    func testSub() {
        XCTAssertEqual(vjArcLibrary.sub(a: 2, b: 1), 1)
    }

}
